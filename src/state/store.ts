import { configureStore } from '@reduxjs/toolkit'

import pokemonList from './pokemonList'
import view from './view'
import { pokemonApi } from './services/pokemon'

export const store = configureStore({
  reducer: {
    pokemonList,
    [pokemonApi.reducerPath]: pokemonApi.reducer,
    view
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(pokemonApi.middleware)
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch