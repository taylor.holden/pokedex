import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction} from '@reduxjs/toolkit'

import type { PokemonListItem } from '../types'

interface ViewState {
    selectedPokemon: PokemonListItem | null
}

const initialState = { selectedPokemon: null } as ViewState

const viewSlice = createSlice({
  name: 'pokemonList',
  initialState,
  reducers: {
    setSelectedPokemon(state, action: PayloadAction<PokemonListItem>) {
      state.selectedPokemon = action.payload
    }
  }
})

export const { setSelectedPokemon } = viewSlice.actions
export default viewSlice.reducer