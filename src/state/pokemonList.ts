import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction} from '@reduxjs/toolkit'

import type { PokemonListItem } from '../types'

interface PokemonListState {
    list: PokemonListItem[],
    loading: boolean
}

const initialState = { list: [], loading: false } as PokemonListState

const pokemonListSlice = createSlice({
  name: 'pokemonList',
  initialState,
  reducers: {
    setList(state, action: PayloadAction<PokemonListItem[]>) {
      state.list = action.payload
    },
    toggleLoading(state) {
      state.loading = !state.loading
    }
  }
})

export const { setList, toggleLoading } = pokemonListSlice.actions
export default pokemonListSlice.reducer