export type Page<T> = {
    count: number,
    next: string,
    previous: string,
    results: T[]
}

export interface ListItem {
    name: string
}