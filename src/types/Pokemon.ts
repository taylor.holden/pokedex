export type Ability = {
    name: string,
    url: string
}

export type AbilityListItem = {
    is_hidden: boolean,
    slot: number,
    ability: Ability
}

export type Move = {
    name: string,
    url: string
}

export type MoveLearnMethod = {
    name: string,
    url: string
}

export type VersionGroupDetails = {
    move_learn_method: MoveLearnMethod
}

export type MoveListItem = {
    move: Move,
    version_group_details: VersionGroupDetails[]
}

export type Sprites = {
    'front_default': string,
    'front_female': string,
    'front_shiny': string,
    'front_shiny_female': string,
}

export type Type = {
    name: string,
    url: string
}

export type TypeListItem = {
    slot: number,
    type: Type
}

export type Pokemon = {
    id: number,
    name: string,
    // "base_experience": 113,
    height: number,
    // "is_default": true,
    order: number,
    weight: number,
    'abilities': AbilityListItem[],
    // "forms": [
    //     {
    //         "name": "clefairy",
    //         "url": "https://pokeapi.co/api/v2/pokemon-form/35/"
    //     }
    // ],
    // "game_indices": [
    //     {
    //         "game_index": 35,
    //         "version": {
    //             "name": "white-2",
    //             "url": "https://pokeapi.co/api/v2/version/22/"
    //         }
    //     }
    // ],
    // "held_items": [
    //     {
    //         "item": {
    //             "name": "moon-stone",
    //             "url": "https://pokeapi.co/api/v2/item/81/"
    //         },
    //         "version_details": [
    //             {
    //                 "rarity": 5,
    //                 "version": {
    //                     "name": "ruby",
    //                     "url": "https://pokeapi.co/api/v2/version/7/"
    //                 }
    //             }
    //         ]
    //     }
    // ],
    // "location_area_encounters": "/api/v2/pokemon/35/encounters",
    'moves': MoveListItem[],
    // "species": {
    //     "name": "clefairy",
    //     "url": "https://pokeapi.co/api/v2/pokemon-species/35/"
    // },
    'sprites': Sprites,
    // "stats": [
    //     {
    //         "base_stat": 35,
    //         "effort": 0,
    //         "stat": {
    //             "name": "speed",
    //             "url": "https://pokeapi.co/api/v2/stat/6/"
    //         }
    //     }
    // ],
    'types': TypeListItem[],
    // "past_types": [
    //     {
    //         "generation": {
    //             "name": "generation-v",
    //             "url": "https://pokeapi.co/api/v2/generation/5/"
    //         },
    //         "types": [
    //             {
    //                 "slot": 1,
    //                 "type": {
    //                     "name": "normal",
    //                     "url": "https://pokeapi.co/api/v2/type/1/"
    //                 }
    //             }
    //         ]
    //     }
    // ]
}

export type PokemonListItem = {
    name: string,
    url: string
}
