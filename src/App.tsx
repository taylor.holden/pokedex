import React from 'react'
import './App.css'
import { store } from './state/store'
import { Provider } from 'react-redux'

import Pokedex from './components/Pokedex'

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Pokedex />
      </Provider>
    </div>
  )
}

export default App
