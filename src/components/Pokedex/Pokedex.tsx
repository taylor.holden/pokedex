import React, { useEffect } from 'react'

import { useAppDispatch, useAppSelector } from '../../hooks'
import { setList, toggleLoading } from '../../state/pokemonList'
import { setSelectedPokemon } from '../../state/view'
import {Page, PokemonListItem} from '../../types'
import Search from '../Search'
import Spinner from '../Spinner'

import './Pokedex.css'
import PokemonViewer from './PokemonViewer'
import PokemonMoveListViewer from './PokemonMoveListViewer'

function Pokedex() {
  const pokemonList = useAppSelector(state => state.pokemonList.list)
  const listLoading = useAppSelector(state => state.pokemonList.loading)
  const selectedPokemon = useAppSelector(state => state.view.selectedPokemon)
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(toggleLoading())
    fetchFullList().then((list: PokemonListItem[]) => {
      dispatch(setList(list))
      dispatch(toggleLoading())
    })
  }, [setList, toggleLoading])

  return (
    <div className="Pokedex">
      <div className="Pokedex_TopBar">
        <div className="Pokedex_Facecam_Lens"></div>
        <div className="Pokedex_TopBar_Padding"></div>
      </div>
      <div className="Pokedex_Bezel">
        <div className="Pokedex_Screen MainDisplay">
          {listLoading && <Spinner />}
          {!listLoading && <Search options={pokemonList} onSelect={selection => {
            dispatch(setSelectedPokemon(selection))
          }} />}
          {selectedPokemon && <PokemonViewer />}
        </div>
      </div>
      <div className="Pokedex_Bezel">
        <PokemonMoveListViewer />
      </div>
    </div>
  )
}

async function fetchFullList(): Promise<PokemonListItem[]> {
  let pageNumber: number | null = 0
  let list: (PokemonListItem[]) = []

  while(pageNumber != null) {
    const page: Page<PokemonListItem> = await fetchPageOfPokemonListItems(pageNumber)
    list = [...list, ...page.results]

    if (page.next !== null) {
      pageNumber++
    } else {
      pageNumber = null
    }
  }

  return list
}

function fetchPageOfPokemonListItems(page: number): Promise<Page<PokemonListItem>> {
  return fetch(`https://pokeapi.co/api/v2/pokemon?limit=100&offset=${page * 100}`)
    .then(response => response.json())
}

export default Pokedex
