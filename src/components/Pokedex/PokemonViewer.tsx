import React from 'react'

import { useAppSelector } from '../../hooks'
import {useGetPokemonByNameQuery} from '../../state/services/pokemon'
import Spinner from '../Spinner'

interface Props {}

function PokemonViewer() {
  const selectedPokemon = useAppSelector(state => state.view.selectedPokemon)

  return (
    <div className="Pokedex_Viewer">
      {selectedPokemon && <PokemonSprite name={selectedPokemon.name} />}
    </div>
  )
}

interface SpriteProps {
    name: string
}
function PokemonSprite({
  name
}: SpriteProps) {
  const { data, isFetching } = useGetPokemonByNameQuery(name)

  return (
    (data && !isFetching) ? <img className="Pokedex_Viewer_Sprite" src={data.sprites.front_default} /> : <Spinner />
  )
}

export default PokemonViewer