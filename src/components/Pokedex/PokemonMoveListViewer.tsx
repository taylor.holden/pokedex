import React, { useEffect, useState } from 'react'

import { useAppSelector } from '../../hooks'
import {useGetPokemonByNameQuery} from '../../state/services/pokemon'
import {MoveListItem} from '../../types'

interface Props {}

function PokemonMoveListViewer() {
  const selectedPokemon = useAppSelector(state => state.view.selectedPokemon)

  return (
    <div className="Pokedex_MoveListViewer">
      {selectedPokemon && <MoveList name={selectedPokemon.name} />}
    </div>
  )
}

interface MoveListProps {
    name: string
}
function MoveList({
  name
}: MoveListProps) {
  const { data, isLoading } = useGetPokemonByNameQuery(name)
  const [levelupMoves, setLevelupMoves] = useState<MoveListItem[]>([])
  const [eggMoves, setEggMoves] = useState<MoveListItem[]>([])
  const [machineMoves, setMachineMoves] = useState<MoveListItem[]>([])
  const [tutorMoves, setTutorMoves] = useState<MoveListItem[]>([])

  useEffect(() => {
    if (!data) {
      return
    }

    const newLevelupMoves: MoveListItem[] = []
    const newEggMoves: MoveListItem[] = []
    const newMachineMoves: MoveListItem[] = []
    const newTutorMoves: MoveListItem[] = []

    // @ts-ignore
    data.moves.forEach(move => {
      switch(move.version_group_details[0].move_learn_method.name) {
      case 'egg':
        newEggMoves.push(move)
        return
      case 'machine':
        newMachineMoves.push(move)
        return
      case 'tutor':
        newTutorMoves.push(move)
        return
      case 'level-up':
      default:
        newLevelupMoves.push(move)
      }
    })

    setLevelupMoves(newLevelupMoves)
    setEggMoves(newEggMoves)
    setMachineMoves(newMachineMoves)
    setTutorMoves(newTutorMoves)
  }, [data, setLevelupMoves, setEggMoves, setMachineMoves, setTutorMoves])

  return (
    <div className="Pokedex_MoveList Pokedex_Screen">
      {data && !isLoading && (
        <>
          {levelupMoves.length && <MoveListGroup label="Level Up" moves={levelupMoves} />}
          {eggMoves.length && <MoveListGroup label="Egg Moves" moves={eggMoves} />}
          {machineMoves.length && <MoveListGroup label="TMs / HMs" moves={machineMoves} />}
          {tutorMoves.length && <MoveListGroup label="Tutor Moves" moves={tutorMoves} />}
        </>
      )}
    </div>
  )
}

interface MoveListGroupProps {
    label: string,
    moves: MoveListItem[]
}
function MoveListGroup({
  label,
  moves
}: MoveListGroupProps) {
  return (
    <>
      <div>---{label}---</div>
      {moves.map(move => <div key={move.move.name}>{move.move.name}</div>)}
    </>
  )
}

export default PokemonMoveListViewer