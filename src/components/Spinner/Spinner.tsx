import React from 'react'

import logo from '../../logo.svg'

import './Spinner.css'

function Spinner() {
  return (
    <img src={logo} className="Spinner" />
  )
}

export default Spinner