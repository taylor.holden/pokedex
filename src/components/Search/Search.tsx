import React, { useCallback, useEffect, useState } from 'react'
import './Search.css'

import { ListItem } from '../../types'

interface Props<T> {
    options: T[],
    onSelect?: (selection: T) => void
}

const Search = <T extends ListItem>({
  options,
  onSelect
}: Props<T>) => {
  const [open, setOpen] = useState<boolean>(false)
  const [searchText, setSearchText] = useState<string>('')
  const [suggestions, setSuggestions] = useState<T[]>([])
  const [selection, setSelection] = useState<string>('Search...')
  const [searchHistory, setSearchHistory] = useState<string[]>(loadSearchHistory())

  const appendSearchToHistory = useCallback((search: string) => {
    const indexOfExistingEntry = searchHistory.indexOf(search)
    let newHistory = [...searchHistory, search]
    if (indexOfExistingEntry >= 0) {
      newHistory.splice(indexOfExistingEntry, 1)
    }

    if (newHistory.length > 5) {
      newHistory = newHistory.slice(newHistory.length - 5)
    }

    setSearchHistory(newHistory)
    saveSearchHistory(newHistory)
  }, [searchHistory, setSearchHistory])

  const handleFocusChange = useCallback((event: React.FocusEvent<HTMLDivElement>) => {
    setOpen(!open)
  }, [open, setOpen])

  const handleOnChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value)
  }, [setSearchText, setSelection])

  const handleSelection = useCallback((option: T) => {
    setOpen(false)
    setSearchText('')
    setSelection(option.name)
    appendSearchToHistory(option.name)

    if (onSelect) {
      onSelect(option)
    }
  }, [setOpen, setSearchText, onSelect, appendSearchToHistory, setSelection])

  const handleHistorySelection = useCallback((previousSearch: string) => {
    const selection = options.find(option => option.name.match(previousSearch))

    if (selection) {
      handleSelection(selection)
    }
  }, [options, handleSelection])

  useEffect(() => {
    // need to debounce this
    setSuggestions(options.filter(option => option.name.match(searchText.toLowerCase())).slice(0, 5))
  }, [options, searchText, setSuggestions])

  return (
    <div className="Search" onFocus={handleFocusChange}>
      <input type="text" placeholder={selection} value={searchText} onChange={handleOnChange} />
      {open && suggestions && suggestions.length > 0 && (
        <div className="Search_Suggestions">
          {searchHistory.map((previousSearch, index) => (
            <div key={index} onClick={() => handleHistorySelection(previousSearch)}>⌚{previousSearch}</div>
          ))}
          {suggestions.map(option => (
            <div key={option.name} onClick={() => handleSelection(option)} className="Search_Suggestions_Suggestion">{option.name}</div>
          ))}
        </div>
      )}
    </div>
  )
}

function loadSearchHistory(): string[] {
  let history: string | null = localStorage.getItem('searchHistory')

  if (history) {
    return history.split('|')
  }

  return []
}

function saveSearchHistory(history: string[]): void {
  localStorage.setItem('searchHistory', history.join('|'))
}

export default Search
